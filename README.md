# Dmytro Kyrychuk Blog

## Rebuild automatically on file changes

```console
$ ag -l | entr nikola build
```

## `nikola` bash autocomplete

```console
$ nikola tabcompletion --hardcode-tasks > $VIRTUAL_ENV/bin/_nikola_bash
$ echo 'source $VIRTUAL_ENV/bin/_nikola_bash' >> .venv/bin/activate
```

Rerun the first command when tasks change (e.g., after installing plugins).
